﻿using System;
using UnityEngine;

[Serializable]
public class PickupSave : Save
{

    public Data data;
    private GemPickup pickup;
    private string jsonString;

    //the data that will be written to the save file
    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
       // public Vector3 eulerAngles;
       // public Vector3 destination;
    }

    void Awake()
    {
        pickup = GetComponent<GemPickup>();
        data = new Data();
    }

    //Save 
    public override string Serialize()
    {
        //take the tank's name, position, rotation, and destination, and write them to the json file
        data.prefabName = prefabName;
        data.position = pickup.transform.position;
       
       
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //Load
    public override void Deserialize(string jsonData)
    {
        //load the tank's position, rotation, and destination, and set the parameters accordingly for the live object
        JsonUtility.FromJsonOverwrite(jsonData, data);
        pickup.transform.position = data.position;

        //
        pickup.name = "Pickup";
    }
}