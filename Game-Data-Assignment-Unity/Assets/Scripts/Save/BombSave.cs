﻿using System;
using UnityEngine;

[Serializable]
public class BombSave : Save
{

    public Data data;
    private Bomb bomb;
    private string jsonString;

    //the data that will be written to the save file
    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
        public Vector3 scale;
        public bool explodeState;
        public float timer;

    }

    void Awake()
    {
        bomb = GetComponent<Bomb>();
        data = new Data();
    }

    //Save 
    public override string Serialize()
    {
        //take the tank's name, position, rotation, and destination, and write them to the json file
        data.prefabName = prefabName;
        data.position = bomb.transform.position;
        data.explodeState = bomb.danger;
        data.timer = bomb.currentTimer;
        data.scale = bomb.transform.localScale;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //Load
    public override void Deserialize(string jsonData)
    {
        //load the tank's position, rotation, and destination, and set the parameters accordingly for the live object
        JsonUtility.FromJsonOverwrite(jsonData, data);
        bomb.transform.position = data.position;
        bomb.danger = data.explodeState;
        bomb.currentTimer = data.timer;
        bomb.transform.localScale = data.scale;
        bomb.name = "Bomb";
    }
}




