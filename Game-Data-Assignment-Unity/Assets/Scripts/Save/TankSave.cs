﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save
{

    public Data data;
    private Tank tank;
    private string jsonString;

    //the data that will be written to the save file
    [Serializable]
    public class Data : BaseData {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    void Awake()
    {
        tank = GetComponent<Tank>();
        data = new Data();
    }

    //Save 
    public override string Serialize()
    {
        //take the tank's name, position, rotation, and destination, and write them to the json file
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //Load
    public override void Deserialize(string jsonData)
    {
        //load the tank's position, rotation, and destination, and set the parameters accordingly for the live object
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}