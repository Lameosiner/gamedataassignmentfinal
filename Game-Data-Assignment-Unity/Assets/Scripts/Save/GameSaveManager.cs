﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }


    //all saveable items added to a list
    public void AddObject(string item)
    {
        saveItems.Add(item);
    }

    public void Save()
    {
        //when the function is called, first clear the list of saveable items
        saveItems.Clear();

        //then find all saveable objects and add them to the list anew
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects)
        {
            saveItems.Add(saveableObject.Serialize());
        }

        //write all saveable items to the save file
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath))
        {
            foreach (string item in saveItems)
            {
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    public void Load()
    {
        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (firstPlay) return;

        //if a save is being loaded, destroy all the existing saveable objects in the scene
        //then recreate them from the save file
        LoadSaveGameData();
        DestroyAllSaveableObjectsInScene();
        CreateGameObjects();
    }

    //go through the save file and read each line
    //add the items to the list of saveable items
    void LoadSaveGameData()
    {
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
            while (gameDataFileStream.Peek() >= 0) {
                string line = gameDataFileStream.ReadLine().Trim();
                if (line.Length > 0) {
                    saveItems.Add(line);
                }
            }
        }
    }

    //find all the objects in scene that are saveable and destroy them so they can be re-created
    void DestroyAllSaveableObjectsInScene()
    {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            Destroy(saveableObject.gameObject);
        }
    }

    //re-instantiate all saveable prefabs based on data from the save file
    void CreateGameObjects()
    {
        foreach (string saveItem in saveItems) {
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
