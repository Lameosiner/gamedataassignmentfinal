﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{

    public float maxTimer;
    public float dangerTime;
    public float currentTimer; //to save
    public bool danger; //to save
    bool explode;

    //public GameObject explosionParticles;

    public Color defaultColor;
    public Color dangerColor;

    public float moveSpeed;

    public Vector3 defaultScale;
    public Vector3 explodingScale;

    public GameObject Tank;

    void Awake()
    {
        GetComponent<SpriteRenderer>().material.color = defaultColor;
        Tank = GameObject.FindGameObjectWithTag("Player");

        //defaultScale = new Vector3(1, 1, 1);

    }

    void Update()
    {

        if(Tank == null)
        {
            Tank = GameObject.FindGameObjectWithTag("Player");

        }

        MoveBomb(Tank);

        if(explode == false)
        {
            transform.localScale = defaultScale;
            currentTimer += Time.deltaTime;

            //Danger threshold to change colour
            if(currentTimer >= dangerTime)
            {
                danger = true;

                GetComponent<SpriteRenderer>().material.color = dangerColor;
                transform.localScale = explodingScale;

            }

            //
            if (currentTimer >= maxTimer)
            {
                explode = true;
               // Destroy(gameObject);
            }

        }


    }

    void MoveBomb(GameObject target)
    {
        transform.Translate((target.transform.position - transform.position) * moveSpeed * Time.deltaTime);
    }

}
